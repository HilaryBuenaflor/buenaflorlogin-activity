package com.example.hilarybuenaflor.loginactivitybuenaflor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Button;
import android.widget.Toast;
import android.content.Intent;

public class MainActivity extends AppCompatActivity {
    Button LoginBtn;
    EditText Username, Password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Username = (EditText) findViewById(R.id.editText);
        Password = (EditText) findViewById(R.id.editText2);
        LoginBtn = (Button) findViewById(R.id.button);
    }

    public void LoginBtn(View v){
        Toast.makeText(getApplicationContext(), "Logging in", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, NextActivity.class);
        intent.putExtra("Username", Username.getText().toString());
        intent.putExtra("Password", Password.getText().toString());
        startActivity(intent);
    }
}
