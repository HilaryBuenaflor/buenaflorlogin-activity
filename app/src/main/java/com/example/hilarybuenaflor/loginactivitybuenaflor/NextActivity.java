package com.example.hilarybuenaflor.loginactivitybuenaflor;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class NextActivity extends AppCompatActivity {
    TextView Username2, Password2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_next);

        Username2 = (TextView) findViewById(R.id.textView3);
        Password2 = (TextView) findViewById(R.id.textView5);

        Username2.setText(getIntent().getStringExtra("Username"));
        Password2.setText(getIntent().getStringExtra("Password"));
    }
}
